class PrimaryReplicaRouter(object):

    def db_for_read(self, model, **hints):
        """
        All other reads go to a randomly-chosen replica.
        """
        return 'cf_comment_slave'

    def db_for_write(self, model, **hints):
        """
        Writes always go to primary.
        """
        return 'cf_comment_db'

    def allow_relation(self, obj1, obj2, **hints):
        """
        Relations between objects are allowed if both objects are in the primary/replica pool.
        """
        db_list = ('cf_comment_db', 'cf_comment_slave')
        if obj1._state.db in db_list and obj2._state.db in db_list:
            return True
        """
        Allow relations if a model in the django base apps is involved.
        """
        if obj1._meta.app_label in self.django_app_labels or obj2._meta.app_label in self.django_app_labels:
           return True

        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        All non-auth models end up in this pool.
        """
        return True
