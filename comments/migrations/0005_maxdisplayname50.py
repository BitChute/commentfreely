from django.db import migrations, models

class Migration(migrations.Migration):

    dependencies = [
        ('comments', '0004_downvotes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='display_name',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
