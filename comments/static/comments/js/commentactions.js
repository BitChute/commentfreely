function initComments(cf_url, cf_auth, currentUserId, profilePictureURL, commentCount, refreshAction, isThreadAdmin) {
	window.commentCount=commentCount;
		$('#comments-container').comments({
			enableAttachments: true,
			readOnly: false,
			currentUserId: currentUserId,
			profilePictureURL: profilePictureURL,
			enableDeletingCommentWithReplies: true,
			currentUserIsAdmin: isThreadAdmin,
			textareaMaxRows:10,
			getComments: function(success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/get_comments/', 
					data: { cf_auth: cf_auth, commentCount: window.commentCount, isNameValuesArrays: true }, 
					success: function(comments) { success(comments); window.commentCount = comments.values.length; }, 
					error: error 
				});
			},
			postComment: function(data, success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/create_comment/',
					data: { commentData: data, cf_auth: cf_auth },
					success: function(userArray) { success(userArray); window.commentCount++; },
					error: error
				});
			},
			putComment: function(data, success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/update_comment/',
					data: { commentData: data, cf_auth: cf_auth },
					success: function (updated) { success(updated) },
					error: error
				});
			},
			deleteComment: function(data, success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/delete_comment/',
					data: { commentData: data, cf_auth: cf_auth },
					success: function(userArray) { window.commentCount--; },
					error: error
				});
				success();
			},
			voteOnComment: function(data, success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/vote_on_comment/',
					data: { commentData: data, cf_auth: cf_auth },
					success: success,
					error: error
				});
			},
			flagComment: function(data) {
                $.ajax({ 
                    type: 'post', 
                    url: cf_url+'/api/flag_comment/',
                    data: { commentData: data, cf_auth: cf_auth },
                });
			},
			timeFormatter: function(time) {
    			return moment(time).fromNow();
  			},
  			refreshAction,
		});
}
