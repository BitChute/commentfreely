from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest, HttpResponseForbidden, Http404
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.utils.crypto import get_random_string
from django.contrib.auth.decorators import login_required
from django.dispatch import Signal
from django.dispatch import receiver
from comments.signals import *
from comments.constants import Constants
from comments.models import Thread, Comment, Vote, Profile, Flag
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.conf import settings
from urllib.parse import urlparse
import uuid
import re
import base64, datetime, re, logging, mimetypes, hmac, hashlib, json
from django.views.decorators.cache import cache_page
from django.core.cache import cache
from django.db.models import F

logger = logging.getLogger(__name__)

permitted_http_request_methods = ["POST"]

badRequest = HttpResponseBadRequest("")

"""
================================================================================================================================================================
KEY NAMES
================================================================================================================================================================
"""

ATTACHMENTS_KEY = "attachments"
AUTHOR_KEY = "author"
CF_AUTH_KEY = "cf_auth"
CONTENT_KEY = "content"
CREATOR_KEY = "creator"
CREATED_KEY = "created"
CREATED_BY_ADMIN_KEY = "created_by_admin"
CREATED_BY_CURRENT_USER_KEY = "created_by_current_user"
DATE_CREATED_KEY = "date_created"	
DISPLAY_NAME_KEY = "display_name"
DOWN_VOTE_COUNT_KEY = "down_vote_count"
FILE_KEY = "file"
FILE_URL_KEY = "file_url"
FILE_MIME_TYPE_KEY = "file_mime_type"
FLAGGER_KEY = "flagger"
FLAG_REASON_KEY = "flag_reason"
FULLNAME_KEY = "fullname"
ID_KEY = "id"
ICON_URL_KEY = "icon_url"
IS_NEW_KEY = "is_new"
LAST_UPDATED_KEY_NAME = "last_updated"
MIME_TYPE_KEY = "mime_type"
MODIFIED_KEY = "modified"
OWNER_ID_KEY = "owner_id"
PARENT_KEY = "parent"
PINGS_KEY = "pings"
PROFILE_ID_KEY = "profile_id"
PROFILE_PICTURE_URL_KEY = "profile_picture_url"
THREAD_ID_KEY = "thread_id"
UP_VOTE_COUNT_KEY = "up_vote_count"
USER_HAS_UPVOTED_KEY = "user_has_upvoted"
USER_VOTE_KEY = "user_vote"

"""
================================================================================================================================================================
COMMENT API ENDPOINTS
================================================================================================================================================================
"""

def get_cf_json(request):
	# Get "cf_auth" request param (raise exception, where missing)
	try:
		cf_auth = request.POST[CF_AUTH_KEY]
	except:
		raise KeyError("Missing \"" + CF_AUTH_KEY + "\" request param!")
		
	# Define required id keys
	required_ids = [PROFILE_ID_KEY, THREAD_ID_KEY]

	# Extract encoded cf_json dictionary from "cf_auth" request param, and validate dictionary keys
	try:
		cf_json = json.loads(base64.b64decode(cf_auth.split()[0]).decode('utf-8'))

		# Where id key isn't a string, or is empty, or exceeds maximum length, flag invaild (see exception trap for remaining errors)
		for key in required_ids:
			if type(cf_json[key]) is not str or len(cf_json[key].strip()) == 0 or len(cf_json[key]) > Constants.MAX_KEY_LENGTH:
				cf_json = None

	# On exception, flag invalid request (raised for any of the following: missing "cf_auth" param, bad encoding, not dictionary and missing keys)
	except:
		cf_json = None

	# If inavlid request, raise exception (intentionally don't include "cf_auth" param, to avoid logging intentionally long garbage text)
	if not cf_json:
		raise ValueError("Invalid cf_json dictionary!")

	# Return cf_json dictionary, profile_id and thread_id values
	return cf_json, cf_json[PROFILE_ID_KEY], cf_json[THREAD_ID_KEY]

def is_authorized(request):
	try:
		auth_items = request.POST[CF_AUTH_KEY].split()
		cf_data = auth_items[0]
		cf_signature = auth_items[1]
		cf_timestamp = auth_items[2]
		cf_message = cf_data + ' ' + str(cf_timestamp)
		cf_verify_signature = hmac.HMAC(settings.API_SECRET.encode('utf-8'), cf_message.encode('utf-8'), hashlib.sha256).hexdigest()

		if cf_signature != cf_verify_signature:
			logger.error(CF_AUTH_KEY + " signature mismatch!")
			return False
	except Exception as e:
		logger.error(e)
		return False

	return True

def init(cf_json, profile_id, thread_id):
	profile = None
	if profile_id != "anonymous" and profile_id is not None:
		try:
			profile = Profile.objects.get(pk=profile_id)
			change=False
			if getattr(profile, DISPLAY_NAME_KEY) != cf_json[DISPLAY_NAME_KEY]:
				setattr(profile, DISPLAY_NAME_KEY, cf_json[DISPLAY_NAME_KEY])
				change=True
			if getattr(profile, ICON_URL_KEY) != cf_json[ICON_URL_KEY]:
				setattr(profile, ICON_URL_KEY, cf_json[ICON_URL_KEY])
				change=True
			if change:
				profile.save()
		except ObjectDoesNotExist:
			logger.warn('profile does not exist: ' + profile_id)
			profile = Profile(pk=profile_id, display_name=cf_json[DISPLAY_NAME_KEY], icon_url=cf_json[ICON_URL_KEY])
			profile.save()

	try:
		owner = Profile.objects.get(pk=cf_json[OWNER_ID_KEY])
	except ObjectDoesNotExist:
		owner = Profile(pk=cf_json[OWNER_ID_KEY])
		owner.save()

	try:
		thread = Thread.objects.get(pk=thread_id)
	except ObjectDoesNotExist:
		thread = Thread(pk=thread_id, owner=owner)
		thread.save()

	return profile, thread
		
@csrf_exempt
@require_http_methods(permitted_http_request_methods)
def get_comments(request):

	# Extract "cf_json" dictionary from encoded "cf_auth" request param, ensuring "profile_id" (id of logged in user, if any) and "thread_id" (id of associated video etc) primary values are present, and valid (returned)
	try:
		cf_json, profile_id, thread_id = get_cf_json(request)
	except Exception as e:
		logger.error(e)
		return badRequest

	# Get profile and thread objects, creating objects where missing (also owner), saving changes to profile ("display_name" and/or "icon_url")
	try:
		profile, thread = init(cf_json, profile_id, thread_id)
	except Exception as e:
		logger.critical(e)
		return badRequest
	
	# Get comments for thread from cache, or database, re-caching where not in cache (including current comment count, aborting on error)
	comments_list = cache.get(thread_id)
	if not comments_list:
		try:
			comments_list = list(Comment.objects.filter(thread=thread_id, state__gte=Constants.STATE_VISIBLE).order_by(DATE_CREATED_KEY))
			cache.set(thread_id, comments_list, Constants.CACHE_TIMEOUT)
			cache.set('count_'+thread_id, len(comments_list), Constants.CACHE_TIMEOUT)
			logger.info("Retrieved thread (" + thread_id + ") comments: " + str(len(comments_list)))
		except Exception as e:
			logger.error(e)
			return badRequest

	# Create lookup for comments user has voted on (if any) since thread was created (significantly better would be if Vote table had thread foreign key, but at least date normally significantly limits entries returned)
	user_vote_lookup = { }
	if profile:
		try:
			# for vote in Vote.objects.filter(voter=profile_id, thread=thread_id):
			for vote in Vote.objects.filter(voter=profile_id, date_created__gte=getattr(thread, DATE_CREATED_KEY)):
				user_vote_lookup[vote.comment_id] = vote.vote # note adding "_id" to foreign key name yields associated id, without retrieving foreign entry
		except Exception as e:
			logger.warn(e)

	# Construct client comments array
	client_comment_count = int(request.POST.get('commentCount', "0"))
	is_name_values_arrays = request.POST.get("isNameValuesArrays");

	comment_fields = [ID_KEY, PARENT_KEY, CREATED_KEY, MODIFIED_KEY, CONTENT_KEY, PINGS_KEY, CREATOR_KEY, FULLNAME_KEY, CREATED_BY_ADMIN_KEY, CREATED_BY_CURRENT_USER_KEY, UP_VOTE_COUNT_KEY, DOWN_VOTE_COUNT_KEY, USER_VOTE_KEY, IS_NEW_KEY, \
		PROFILE_PICTURE_URL_KEY, ATTACHMENTS_KEY];

	comments_values=[]

	comments_array=[] # Temp: for backwards compatability

	for i, c in enumerate(comments_list):
		created_by_current_user = (c.creator_id == profile_id)

		user_vote = user_vote_lookup.get(c.comment_id)

		modified = str(c.modified) if c.modified else None
		
		content = c.content
		if c.state == Constants.STATE_PLACEHOLDER:
			created_by_current_user = False
			content = "[comment removed]"

		is_new = (client_comment_count != 0 and i >= client_comment_count)

		if is_name_values_arrays:
			comment_data = [ c.comment_id, c.parent, str(getattr(c, DATE_CREATED_KEY)), modified, content, [], c.creator_id, getattr(c.creator, DISPLAY_NAME_KEY), False, created_by_current_user, getattr(c, UP_VOTE_COUNT_KEY), \
				getattr(c, DOWN_VOTE_COUNT_KEY), user_vote, is_new, getattr(c.creator, ICON_URL_KEY) ]
		else:
			comment = { ID_KEY: c.comment_id, PARENT_KEY: c.parent, CREATED_KEY: str(getattr(c, DATE_CREATED_KEY)), MODIFIED_KEY: modified, CONTENT_KEY: content, PINGS_KEY: [], CREATOR_KEY: c.creator_id, \
				FULLNAME_KEY: getattr(c.creator, DISPLAY_NAME_KEY), CREATED_BY_ADMIN_KEY: False, CREATED_BY_CURRENT_USER_KEY: created_by_current_user, "upvote_count": getattr(c, UP_VOTE_COUNT_KEY), \
				USER_HAS_UPVOTED_KEY: user_vote is True, IS_NEW_KEY: is_new, PROFILE_PICTURE_URL_KEY: getattr(c.creator, ICON_URL_KEY) }

		attachments = []
		if c.file_url and c.state != Constants.STATE_PLACEHOLDER:
			attachment = {}
			attachment[ID_KEY] = 1
			attachment[FILE_KEY] = c.file_url
			attachment[MIME_TYPE_KEY] = c.file_mime_type
			attachments.append(attachment)
			
		if is_name_values_arrays:
			comment_data.append(attachments)
			comments_values.append(comment_data)
		else:
			comment[ATTACHMENTS_KEY] = attachments
			comments_array.append(comment)
	
	# Respond with comment field names, and comments values, in JSON format
	return JsonResponse({"names": comment_fields, "values": comments_values}) if is_name_values_arrays else JsonResponse(comments_array, safe=False)
	
def get_multimedia_file_url_and_type(content):
	file_url = None
	file_mime_type = None

	try:
		urls = re.findall("https?://[^\s]+", content)
		for url in urls:
			urlComponents = urlparse(url)
			tld = re.sub(r'^\w+\.', '', urlComponents.netloc)
			file_mime_type = mimetypes.guess_type(url)[0] if (len(urlComponents.path) > 1 or len(urlComponents.query) > 0) else None
			if not tld in settings.MEME_DOMAINS or not file_mime_type:
				continue

			# print(domain)
			file_url = url
			logger.info(file_mime_type)
			break
	except Exception as e:
		logger.warn(e)

	# NOTE: only file_url is guaranteed to be None, where there is no supported multimedia file
	
	return file_url, file_mime_type

def get_commentData_value(request, commentDataKey, maxLength=Constants.MAX_KEY_LENGTH):

	key = "commentData[" + commentDataKey + "]"
	value = request.POST[key] 
	valueLength = len(value)

	if valueLength > maxLength:
		raise ValueError("request.POST[\"" + key + "\"] value's length (" + str(valueLength) + ") exceeds max (" + str(maxLength) + ")")

	return value

@csrf_exempt
@require_http_methods(permitted_http_request_methods)
def create_comment(request):

	try:
		cf_json, profile_id, thread_id = get_cf_json(request)

		data = { THREAD_ID_KEY: thread_id, ID_KEY: get_random_string(length=Constants.MAX_KEY_LENGTH), PARENT_KEY: request.POST.get('commentData[parent]',None), \
			CONTENT_KEY: get_commentData_value(request, CONTENT_KEY, Constants.MAX_COMMENT_LENGTH), PROFILE_ID_KEY: profile_id }
	except Exception as e:
		logger.error(e)
		return badRequest

	attachments = []
	
	file_url, file_mime_type = get_multimedia_file_url_and_type(data[CONTENT_KEY])

	if file_url:
		attachment = { }
		data[FILE_URL_KEY] = attachment[FILE_KEY] = file_url
		data[FILE_MIME_TYPE_KEY] = attachment[MIME_TYPE_KEY] = file_mime_type
		attachments.append(attachment)

	if not is_authorized(request) or not signal_new_comment.send_robust(sender=1, data=data)[0][1]:
		return badRequest

	parent = data[PARENT_KEY] if data[PARENT_KEY] else None
		
	comment = { ID_KEY: data[ID_KEY], PARENT_KEY: parent, CREATED_KEY: data[CREATED_KEY], MODIFIED_KEY: None, CONTENT_KEY: data[CONTENT_KEY], PINGS_KEY: [], CREATOR_KEY: profile_id, FULLNAME_KEY: cf_json[DISPLAY_NAME_KEY], \
		PROFILE_PICTURE_URL_KEY: cf_json[ICON_URL_KEY], CREATED_BY_ADMIN_KEY: False, CREATED_BY_CURRENT_USER_KEY: True, IS_NEW_KEY: False, ATTACHMENTS_KEY : attachments }

	if request.POST.get("commentData[" + UP_VOTE_COUNT_KEY + "]"):
		comment[UP_VOTE_COUNT_KEY] = 0
		comment[DOWN_VOTE_COUNT_KEY] = 0
		comment[USER_VOTE_KEY] = None
	else:
		comment["upvote_count"] = 0;
		comment[USER_HAS_UPVOTED_KEY] = False

	return JsonResponse(comment)
	
@csrf_exempt
@require_http_methods(permitted_http_request_methods)
def update_comment(request):
	try:
		cf_json, profile_id, thread_id = get_cf_json(request)

		data = { THREAD_ID_KEY: thread_id, ID_KEY: get_commentData_value(request, ID_KEY), PARENT_KEY: get_commentData_value(request, PARENT_KEY), CONTENT_KEY: get_commentData_value(request, CONTENT_KEY, Constants.MAX_COMMENT_LENGTH), \
			PROFILE_ID_KEY: profile_id }
	except Exception as e:
		logger.error(e)
		return badRequest

	file_url, file_mime_type = get_multimedia_file_url_and_type(data[CONTENT_KEY])

	attachments = []

	if file_url:
		attachment = { }
		data[FILE_URL_KEY] = attachment[FILE_KEY] = file_url
		data[FILE_MIME_TYPE_KEY] = attachment[MIME_TYPE_KEY] = file_mime_type
		attachments.append(attachment)

	if not is_authorized(request) or not signal_update_comment.send_robust(sender=1, data=data)[0][1]:
		return badRequest
	
	return JsonResponse({ATTACHMENTS_KEY: attachments, MODIFIED_KEY: data[MODIFIED_KEY]}) if request.POST.get("commentData[" + UP_VOTE_COUNT_KEY + "]") else JsonResponse(attachments, safe=False)

@csrf_exempt
@require_http_methods(permitted_http_request_methods)
def delete_comment(request):
	try:
		cf_json, profile_id, thread_id = get_cf_json(request)

		author = Profile.objects.get(pk=profile_id)
	except Exception as e:
		logger.error(e)
		return badRequest

	if not author.is_active:
		return badRequest
	
	data = { THREAD_ID_KEY: thread_id, ID_KEY: get_commentData_value(request, ID_KEY), PROFILE_ID_KEY: profile_id }

	if not is_authorized(request) or not signal_delete_comment.send_robust(sender=1, data=data)[0][1]:
		return badRequest
		
	return JsonResponse({'success': True})
	
@csrf_exempt
@require_http_methods(permitted_http_request_methods)	
def vote_on_comment(request):
	try:
		cf_json, profile_id, thread_id = get_cf_json(request)

		author = Profile.objects.get(pk=profile_id)
		user_vote_text = get_commentData_value(request, USER_VOTE_KEY)

		data = { THREAD_ID_KEY: thread_id, ID_KEY: get_commentData_value(request, ID_KEY), AUTHOR_KEY: author, USER_VOTE_KEY : json.loads(user_vote_text) if len(user_vote_text) else None}

		creator_id = get_commentData_value(request, CREATOR_KEY)
	except Exception as e:
		logger.error(e)
		return badRequest

	if not is_authorized(request) or profile_id == creator_id or not signal_vote.send_robust(sender=1, data=data)[0][1]:
		return badRequest

	return JsonResponse({UP_VOTE_COUNT_KEY: data[UP_VOTE_COUNT_KEY], DOWN_VOTE_COUNT_KEY: data[DOWN_VOTE_COUNT_KEY]})

@csrf_exempt
@require_http_methods(permitted_http_request_methods)	
def upvote_comment(request):
	try:
		cf_json, profile_id, thread_id = get_cf_json(request)

		author = Profile.objects.get(pk=profile_id)

		data = { THREAD_ID_KEY: thread_id, ID_KEY: get_commentData_value(request, ID_KEY), AUTHOR_KEY: author, USER_VOTE_KEY: True if get_commentData_value(request, USER_HAS_UPVOTED_KEY) == "true" else None}
	except Exception as e:
		logger.error(e)
		return badRequest

	if not is_authorized(request) or profile_id == request.POST.get('commentData[creator]') or not signal_vote.send_robust(sender=1, data=data)[0][1]:
		return badRequest

	return JsonResponse({'success': True})

@csrf_exempt
@require_http_methods(permitted_http_request_methods)
def flag_comment(request):
	try:
		cf_json, profile_id, thread_id = get_cf_json(request)

		flagger = Profile.objects.get(pk=profile_id)

		data = { ID_KEY: get_commentData_value(request, ID_KEY), FLAGGER_KEY: flagger, FLAG_REASON_KEY: get_commentData_value(request, FLAG_REASON_KEY, Constants.MAX_FLAG_REASON_LENGTH) }
	except Exception as e:
		logger.error(e)
		return badRequest

	if not is_authorized(request) or not signal_flag.send_robust(sender=1, data=data)[0][1]:
		return badRequest

	return JsonResponse({'success': True})

@csrf_exempt
@require_http_methods(permitted_http_request_methods)
def get_comment_count(request):
	cf_thread = request.POST.get('cf_thread', None)

	count = cache.get('count_' + cf_thread) if cf_thread else 0

	return JsonResponse({'commentCount': count if count else 0})

"""
================================================================================================================================================================
COMMENT API RECIEVERS
================================================================================================================================================================
"""

@receiver(signal_new_comment)
def on_new_comment(sender, data, **kwargs):

	thread_id=data[THREAD_ID_KEY]
		
	# Get thread (abort, if missing)
	try:
		thread = Thread.objects.get(pk=thread_id)
	except Exception as e:
		logger.error(e)
		return False

	# If this comment has a parent make sure it is a valid one	
	parent_id = None
	if data[PARENT_KEY]:
		try:
			parent_id=data[PARENT_KEY]
			Comment.objects.get(pk=parent_id, state=Constants.STATE_VISIBLE)
		except ObjectDoesNotExist:
			parent_id = None
		except Exception as e:
			parent_id = None
			logger.warn(e)

	# Get profile (abort, if missing)
	try:
		profile = Profile.objects.get(pk=data[PROFILE_ID_KEY])
	except Exception as e:
		logger.error(e)
		return False

	# If profile isn't active, abort
	if not profile.is_active:
		return False
		
	# Create and save comment entry (abort, on error)
	try:
		comment = Comment(pk=data[ID_KEY], thread=thread, parent=parent_id, content=data[CONTENT_KEY], creator=profile)
	
		if FILE_URL_KEY in data:
			setattr(comment, FILE_URL_KEY, data[FILE_URL_KEY])
			setattr(comment, FILE_MIME_TYPE_KEY, data[FILE_MIME_TYPE_KEY])
			
		comment.save()

		data[CREATED_KEY] = str(getattr(comment, DATE_CREATED_KEY))
	except Exception as e:
		logger.error(e)
		return False

	# Drop thread comments queryset from cache (on error, just log warning)
	try:
		cache.delete(thread_id)
	except Exception as e:
		logger.warn(e)

	# Update cached comment count (on error, just log warning)
	try:
		count = cache.get('count_'+thread_id)
		if count:
			cache.set('count_' + thread_id, count + 1, Constants.CACHE_TIMEOUT)
	except Exception as e:
		logger.warn(e)

	# Success
	return True

		
    
@receiver(signal_update_comment)
def on_update_comment(sender, data, **kwargs):
	logger.info("signal_update_comment")
    
	# Update comment entry (abort, on error)
	try:
		# Get comment entry
		comment = Comment.objects.get(pk=data[ID_KEY], creator=data[PROFILE_ID_KEY])

		# Update content, attachment and modified fields, and save
		is_attachment = FILE_URL_KEY in data
		is_existing_attachment = getattr(comment, FILE_URL_KEY) is not None
		setattr(comment, CONTENT_KEY, data[CONTENT_KEY])
		setattr(comment, FILE_URL_KEY, data[FILE_URL_KEY] if is_attachment else None)
		setattr(comment, FILE_MIME_TYPE_KEY, data[FILE_MIME_TYPE_KEY] if is_attachment else None)
		setattr(comment, MODIFIED_KEY, datetime.datetime.now(tz=timezone.utc))

		updated_fields = [CONTENT_KEY, MODIFIED_KEY, LAST_UPDATED_KEY_NAME]
		if is_attachment or is_existing_attachment:
			updated_fields.extend([FILE_URL_KEY, FILE_MIME_TYPE_KEY])
				
		comment.save(update_fields=updated_fields)

		# Output modified date
		data[MODIFIED_KEY] = str(getattr(comment, MODIFIED_KEY))
	except Exception as e:
		logger.error(e)
		return False

	# Drop thread comments queryset from cache (on error, just log warning)
	try:
		cache.delete(getattr(comment, THREAD_ID_KEY))
	except Exception as e:
		logger.warn(e)

	# Success
	return True
    
    	
@receiver(signal_delete_comment)
def on_delete_comment(sender, data, **kwargs):
	logger.info("signal_delete_comment")

	comment_id = data[ID_KEY]
	profile_id = data[PROFILE_ID_KEY]
    
	# Get comment and it's thread id (abort, on error)
	try:
		comment = Comment.objects.get(pk=comment_id)
		comment_thread_id = getattr(comment, THREAD_ID_KEY)
	except Exception as e:
		logger.error(e)
		return False

	# If comment creator isn't current user, verify comment thread (video) owner is deleting comment (abort, if not, or on error)
	if comment.creator_id != profile_id:
		try:
			# NOTE: by querying for the comment's thread_id, guard against mistmatch with current user's thread 

			if not Thread.objects.filter(pk=comment_thread_id, owner=profile_id).exists():
				logger.warn("Attempt to delete comment (" + comment_id + ") by unaurthorized user (not creator or thread owner): " + profile_id)
				return False
		except Exception as e:
			logger.error(e)
			return False
	
	# Update status of comment (abort, on error)
	try:
		# If comment was ever a parent, flag as place holder (remember, "deleted" descendents are only flagged, not removed)
		if Comment.objects.filter(parent=comment_id).exists():
			comment.state = Constants.STATE_PLACEHOLDER

		# Otherwise, flag as deleted, and decrement cached thread comment count (where present and not zero)
		else:
			comment.state = Constants.STATE_DELETED # TODO: Split out for different deletion reasons

			count = cache.get('count_' + comment_thread_id)
			if count:
				cache.set('count_' + comment_thread_id, count - 1, Constants.CACHE_TIMEOUT)
		# Update comment
		comment.save()
	except Exception as e:
		logger.error(e)
		return False

	# Drop thread comments queryset from cache
	try:
		cache.delete(comment_thread_id)
	except Exception as e:
		logger.warn(e)

	# Success
	return True
    

@receiver(signal_vote)
def on_vote_comment(sender, data, **kwargs):
	comment_id = data[ID_KEY]
	new_vote = data[USER_VOTE_KEY]

	# logger.info("New vote: " + str(new_vote))

	# Get comment entry (abort, if missing)
	try:
		comment = Comment.objects.get(pk=comment_id)
	except Exception as e:
		logger.error(e)
		return False

	# Where existing vote entry, get current vote, and where user is un-voting, drop entry (abort, where new vote matches current vote, and on error)
	try:
		vote_entry = Vote.objects.get(comment=comment, voter=data[AUTHOR_KEY])
		existing_vote = vote_entry.vote
		if new_vote is existing_vote:
			logger.warn("New " + USER_VOTE_KEY + " (" + str(new_vote) + ") matches existing vote (" + str(existing_vote) + ")")
			return False
	# Otherwise (new vote), create vote entry object (abort, where new vote isn't specified)
	except ObjectDoesNotExist:
		if new_vote is None:
			logger.warn("Attempting to remove non-existant vote!")
			return False
		try:
			vote_entry = Vote(comment=comment, voter=data[AUTHOR_KEY])
			existing_vote = None
		except Exception as e:
			logger.error(e)
			return False
	except Exception as e:
		logger.error(e)
		return False

	# Delete / update vote entry (abort, on error)
	try:
		if new_vote is None:
			vote_entry.delete()
		else:
			vote_entry.vote = new_vote
			vote_entry.save()
	except Exception as e:
		logger.error(e)
		return False

	# Increment or decrement comment up / down vote counts (on error, just log warning)
	try:
		# Set to output new (provisional) vote totals
		data[UP_VOTE_COUNT_KEY] = getattr(comment, UP_VOTE_COUNT_KEY) 
		data[DOWN_VOTE_COUNT_KEY] = getattr(comment, DOWN_VOTE_COUNT_KEY)

		# Update vote count(s)
		updated_fields = [LAST_UPDATED_KEY_NAME]

		if new_vote is True or existing_vote is True:
			up_adj = 1 if new_vote else -1
			setattr(comment, UP_VOTE_COUNT_KEY, F(UP_VOTE_COUNT_KEY) + up_adj)
			data[UP_VOTE_COUNT_KEY] += up_adj
			updated_fields.append(UP_VOTE_COUNT_KEY)

		if new_vote is False or existing_vote is False:
			down_adj = 1 if new_vote is False else -1
			setattr(comment, DOWN_VOTE_COUNT_KEY, F(DOWN_VOTE_COUNT_KEY) + down_adj)
			data[DOWN_VOTE_COUNT_KEY] += down_adj
			updated_fields.append(DOWN_VOTE_COUNT_KEY)

		comment.save(update_fields=updated_fields)
	except Exception as e:
		logger.warn(e)

	# Drop thread comments queryset from cache (on error, just log warning)
	try:
		cache.delete(getattr(comment, THREAD_ID_KEY))
	except Exception as e:
		logger.warn(e)

	# Success
	return True


@receiver(signal_flag)
def on_flag_comment(sender, data, **kwargs):
	logger.info('signal_flag')
	if data[FLAG_REASON_KEY] == "":
		return False

	try:
		comment = Comment.objects.get(pk=data[ID_KEY])

		flag = Flag(reason=data[FLAG_REASON_KEY], flagger=data[FLAGGER_KEY], comment=comment)
		flag.save()
	except Exception as e:
		logger.error(e)
		return False

	return True	
